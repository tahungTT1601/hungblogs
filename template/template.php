<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <title>Hung Blog</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="<?php site_url(); ?>/template/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <div class="wrapper">
        <div class="snow">
            <div class="snowflake">
                ❅
            </div>
            <div class="snowflake">
                ❅
            </div>
            <div class="snowflake">
                ❆
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❅
            </div>
            <div class="snowflake">
                ❆
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❅
            </div>
            <div class="snowflake">
                ❆
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
            <div class="snowflake">
                ❄
            </div>
        </div>

        <div class="container topBotomBordersIn">
            <a class="homepage">HOME</a>
            <a class="main-text articles" data-content="articles-content">ARTICLES</a>
            <a class="main-text portfolio" data-content="portfolio-content">PORTFOLIO</a>
            <a class="main-text about" data-content="about-content">ABOUT</a>
            <a class="main-text contact" data-content="contact-content">CONTACT</a>
            <div id="television">
                <div id="monitor">
                    <audio class="audio" id="audio" autoplay controls>
                        <source src="https://opengameart.org/sites/default/files/audio_preview/swing_0.mp3.ogg" type="audio/mp3">
                    </audio>
                    <div id="monitorscreen">
                        <span id="info" class="content about-content" style="display: none;">Những gì tôi có
                            thể nói về tôi chỉ đơn giản rằng tôi là một con người bình thường với hỉ, nộ, ái, ố đầy đủ
                            như mọi người. Ở đây, tại trang web cá nhân này, điều tôi cố gắng làm là chia sẻ những gì
                            tốt đẹp và hữu ích mà tôi có dịp học hỏi và trải nghiệm được từ cuộc sống cũng như những
                            giây phút hạnh phúc mà tôi trải nghiệm được. Bằng việc chia sẻ này, tôi hy vọng có thể
                            truyền cảm hứng cũng như đem đến một chút ít hạnh phúc đến các bạn, những ai đang đọc các
                            bài viết và chia sẻ của tôi. Một ghi chú khác đến các bạn rằng tôi rất hứng thú trong việc
                            thảo luận về đề tài phát triển cá nhân và làm thế nào để có thể hạnh phúc trong cuộc sống
                            này</span>
                        <span id="info" class="content articles-content" style="display: none;">Chúng ta có thể làm
                            tâm mình trở thành một mặt hồ tĩnh lặng để người khác quây quần chung quanh, và họ sẽ thấy
                            được hình bóng của chính họ. Vì vậy, hãy sống một cuộc đời trong sáng hơn, và có lẽ mãnh
                            liệt hơn, với sự tĩnh lặng của mình</span>
                        <span id="info" class="content portfolio-content" style="display: none;">Sống bằng nghề
                            code thuê để kiếm cơm, code hết ngày này qua ngày khác. Nên bàn tay gõ code đã trở nên thuần
                            thục đến mức "tay nhanh hơn não".
                            Tuy chỉ là một thợ code, nhưng mình vẫn luôn mong muốn học hỏi và tìm hiểu để trở thành một
                            lập trình viên thực thụ</span>
                        <span id="info" class="content contact-content" style="display: none; padding-left: 50px; !important">
                            <strong>Name</strong>: TẠ VĂN HƯNG
                            <br>
                            <br>
                            <strong>DOB</strong>: 25/10/1998
                            <br>
                            <br>
                            <strong>Address</strong>: Bắc Giang
                            <br>
                            <br>
                            <strong>Phone</strong>: 0367832744/ 0383665744
                            <br>
                            <br>
                            <strong>Email</strong>: tahung98svtybg@gmail.com
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="profile-card">
            <div class="profile-card-img">
                <img src="https://scontent.fsgn8-1.fna.fbcdn.net/v/t1.6435-9/92380825_2608555576059037_1327725964884443136_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=yGMWHXu72wQAX_pxv7u&tn=gcfkqhnccCJPj0pO&_nc_ht=scontent.fsgn8-1.fna&oh=00_AT96gLIN88J0vYFAfqHf_knmTSqRK_EjooNKOXjo23B2_Q&oe=631E8BBC" alt="">
            </div>
            <div class="profile-card-info">
                <div class="name">
                    <h3 class="animate-charcter">Đây là trang cá nhân của bạn Hưng</h3>
                </div>
                <div class="position">
                    <span>B</span>
                    <span>A</span>
                    <span>C</span>
                    <span>K</span>
                    <span>E</span>
                    <span>N</span>
                    <span>D</span>
                    &nbsp;
                    <span>D</span>
                    <span>E</span>
                    <span>V</span>
                    <span>E</span>
                    <span>L</span>
                    <span>O</span>
                    <span>P</span>
                    <span>E</span>
                    <span>R</span>
                </div>
                <div class="location">
                    <a href="https://goo.gl/maps/LLQXS3QisKrSQUG5A"><i style="color: red;" class="fa-solid fa-map-location"></i></a> Ho Chi Minh city, Vietnam
                </div>
            </div>
            <div class="profile-card-social">
                <a href="https://www.facebook.com/hung.ta.9822/" class="item facebook" target="_blank">
                    <i class="fa-brands fa-facebook-f"></i>
                </a>
                <a href="https://twitter.com/Hungkudo98" class="item twitter" target="_blank">
                    <i class="fa-brands fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/hung.tryndamare/" class="item instagram" target="_blank">
                    <i class="fa-brands fa-instagram"></i>
                </a>
                <a href="https://github.com/tahungTH1603" class="item github" target="_blank">
                    <i class="fa-brands fa-github"></i>
                </a>
                <a href="https://hungcpm.blogspot.com/" class="item website" target="_blank">
                    <i class="fa-solid fa-link"></i>
                </a>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" integrity="sha512-6PM0qYu5KExuNcKt5bURAoT6KCThUmHRewN3zUFNaoI6Di7XJPTMoT6K0nsagZKk2OB4L7E3q1uQKHNHd4stIQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $('.main-text').click(function() {
        var source = document.getElementById('audio');
        source.src = "<?php site_url(); ?>/template/record/romance.mp3";
        document.getElementById("audio").play();
        $('.content').hide();
        $('.' + $(this).data('content')).show();
    });
    $('.about').click(function() {
        var source = document.getElementById('audio');
        source.src = "<?php site_url(); ?>/template/record/romance.mp3";
        document.getElementById("audio").play();
    });
    $('.articles').click(function() {
        var source = document.getElementById('audio');
        source.src = "<?php site_url(); ?>/template/record/garret.mp3";
        document.getElementById("audio").play();
    });  
    $('.portfolio').click(function() {
        var source = document.getElementById('audio');
        source.src = "<?php site_url(); ?>/template/record/david.mp3";
        document.getElementById("audio").play();
    });  
    $('.contact').click(function() {
        var source = document.getElementById('audio');
        source.src = "<?php site_url(); ?>/template/record/profile.mp3";
        document.getElementById("audio").play();
    });
    $('.homepage').click(function() {
        $('.content').hide();
    });
</script>